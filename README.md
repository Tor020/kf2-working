<!-- // vscode-fold=1 -->

#1. Perk Validation ✔️
#2. ZED Boss HP ✔️
#3. Weapon Upgrades
#4. Survivalist
#5. Transitions between components -> general transitiony shit

#6a. Twitter API 
#6b. Zed Damage resistances being applied against active damamge of weapons <----dasbig

#7. Display Armor on Zeds
#8. Affected by Medic Dmg boost for perks checkbox
#9. Demolitionist impact damage
#10. Medic Hemo Dot damage / generla dot damage
redundant test

#### Vue-Cli
- [Full Vue-cli Docs](https://github.com/vuejs/vue-cli/tree/dev/docs)
- [Vue-cli commands](https://github.com/vuejs/vue-cli/blob/dev/docs/cli.md)
- Seems to be some weird way of passing options to webpack with [vue-cli](https://github.com/vuejs/vue-cli/blob/dev/docs/webpack.md#inspecting-the-projects-webpack-config) 
__

#### Pug
- [HTML to Pug](https://html-to-pug.com/)
- [Pug to HTML](https://pughtml.com/)

__

## 1. Core Calculations FIXME:

- ❓️ Verify that numbers are consistent with spreadsheet  
️



- TODO: Check Over Perk Validation calculations for all classes


- Sharpshooter TODO:
- Demolitionist TODO:
- Firebug TODO:

- TODO: Survivalist 

- Demolitionist m16bullet damage separated from grenade damage :✈️


  - ZED state management so that each navigation switch will maintain the state that the user left it in on the previous page. 
  ie: If a player on commando page selects 2p and HOE diff and then switches to sharpshooter, the ZED component should stay on 2p and HOE diff even though it's re-rendered


- TODO: Implement Twitter API tweet functionality


## QoL   FIXME:
 
<!-- check in old  src/sass/localStorageStyles.js  for the configs -->
<!-- Check in the  src/sass/themes for the dark theme css and bootstrap unfuckings  -->
<!--
// https://github.com/vuejs-templates/webpack/issues/604
// https://stackoverflow.com/questions/47893611/conditional-stylesheet-in-vue-component
// https://vuetifyjs.com/en/style/theme 
-->

- TODO: Add Day / Night Themes Sun icon / Moon icon on the sidebar and swap out / remember selection  ----> basically okay on dark theme being ready i think. Need to configure the localstorage
---> Need to figure out how to conditionally reference stylesheets through webpack, or see what vue-cli's methods are
https://github.com/vuejs-templates/webpack/issues/604


- TODO: weapon icons  to appear nnext to text on the table [link](https://wiki.tripwireinteractive.com/index.php?title=Commando_(Killing_Floor_2)) ️  -> Also add the related perk icon to signify what class it is for natively



- TODO: Berzerker Smash = .25 headshot damage on eviscerator https://www.reddit.com/r/killingfloor/comments/65zjge/quick_question_about_zerkers_eviscerator_and/  + Conditional headshot damage showing up with smash being checked
- TODO: Add Rackem up cap for SS
- TODO: mousewheel on hover scrolling increase perk level
- ✔️ Add icons from the wiki page to associated classes  
- ✔️ Text Align Center? 


## Misc / Advanced FIXME:

Add bleed effects 
Add medic perk boost 
Add altfire for microwavegun

- TODO: Create page overlays that when clicking on a weapon, a modal esque thing slides in that has more advanced info about the weapon.
- Bootstrap Tooltips are broken, prolly require boostrap & jquery JS  ❓️ update: fuck.... updateupdate:✔️ EZ
️
<!-- ## Considerations -->


## Sidebar  🔥🔥🔥🔥🔥🔥🔥
- url isn't quite where i want it.  Index.html should be '/' url path ❓️
- Also need to remove the `#` symbol from the url path ❓️
🤬🤬🤬🤬🤬This is dumb bullshit🤬🤬🤬🤬🤬🤬
[stupid](https://stackoverflow.com/questions/1034621/get-the-current-url-with-javascript)
[retarded n](https://developer.mozilla.org/en-US/docs/Web/API/History_API)
[dumb as fuck](https://developer.mozilla.org/en-US/docs/Web/API/URL)
[bullshit](https://developer.mozilla.org/en-US/docs/Web/API/History_API)

- Create kf2calc subreddit for people to leave feedback if comments are  abitch --->> probably just implement comments though.



## Cleanup

  TODO: 
- Remove undefined values from Zed hp calculations. Return the properties from largezeds and smallzeds into one Hp object so that there is nothing left undefined

# Done
  ## 1. Core Calculations FIXME:

  - ✔️ Setup computed calculations  

  - ✔️ Basic Scaffolding 
  - ✔️ Commando            
  - ✔️ Gunslinger  
  - ✔️ FieldMedic 
  - ✔️ Demolitionist 
  - ✔️ Sharpshooeter 
  - ✔️ Support 
  - ✔️ Swat 
  - ✔️ Berzerker 



### Link Dump


 <!-- https://medium.freecodecamp.org/javascript-objects-square-brackets-and-algorithms-e9a2916dc158 -->
 <!-- https://stackoverflow.com/questions/4244896/dynamically-access-object-property-using-variable/24514304#24514304 -->
 <!-- https://toddmotto.com/methods-to-determine-if-an-object-has-a-given-property/ -->
 <!-- https://javascriptweblog.wordpress.com/2010/08/09/variables-vs-properties-in-javascript/ -->
