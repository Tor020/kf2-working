

   time        lvl    charge Mult     Basedmg
 0-0.2          0        x1.0          80
 0.2-0.4        1        x1.7          136
 0.4-0.6        2        x2.4          192
 0.6-0.8        3        x3.1          248
 0.8s-1.0       4        x3.8          304
 1.0s>          5        x4.5          360


BaseDmg is the final number used in perkdmg calculation 

Afterburn (DOT duration)	2.5s
Afterburn (DOT interval)	0.5s
Afterburn (DOT scale, of current damage)	x0.1
Base damage	80
Base radius	100
Charge power (interval to add 1 point of power)	0.2s
Charge power (maximum power, when fully charged)	5
Charge power (time to fully charge)	1s
Damage type (DOT, Ground Fire)	Fire
Damage type (projectile impact)	Explosive
Ground fire (afterburn DOT duration)	5s
Ground fire (afterburn DOT interval)	1s
Ground fire (afterburn DOT scale)	x0.5
Ground fire (damage falloff)	0 (none)
Ground fire (damage interval)	0.5s
Ground fire (damage)	3
Ground fire (duration)	4s
Ground fire (radius)	150
Ground fire (spawned when fully charged)	yes




addl Info  - Ammo consumed, after fully charged	1 per 2s
   - this ammo does NOT provide extra damage	yes


   Weapon charging: Husk Cannon gains 1 point of the charge power per 0.2s of charging. Projectile itself cannot be released right away, instead it fires within 0.4s intervals: 0.4s /0.8s /1.2s. Pressing and releasing the fire button right away will provide no charge power at all by the moment of the projectile release time.	



	
	
	
